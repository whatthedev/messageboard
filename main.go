package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/didip/tollbooth"
	"github.com/didip/tollbooth/limiter"
	"github.com/gorilla/mux"
)

type MessageString struct {
	Message string `json:"message"`
}

type Message struct {
	ID      int    `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

var Messages []Message
var CurrentIndex int

func main() {
	router := mux.NewRouter()
	readLimiter := tollbooth.NewLimiter(1, &limiter.ExpirableOptions{DefaultExpirationTTL: time.Second})
	postLimiter := tollbooth.NewLimiter(2, &limiter.ExpirableOptions{DefaultExpirationTTL: time.Minute})

	Messages = append(Messages, Message{ID: 0, Message: "Hello There"})
	Messages = append(Messages, Message{ID: 1, Message: "General Kenobi"})

	CurrentIndex = len(Messages)

	router.Handle("/messages", tollbooth.LimitFuncHandler(readLimiter, GetMessages)).Methods("GET")
	//router.HandleFunc("/messages", GetMessages).Methods("GET")
	router.Handle("/message/{id}", tollbooth.LimitFuncHandler(readLimiter, GetMessage)).Methods("GET")
	//router.HandleFunc("/message/{id}", GetMessage).Methods("GET")
	router.Handle("/message", tollbooth.LimitFuncHandler(postLimiter, CreateMessage)).Methods("POST")
	//router.HandleFunc("/message", CreateMessage).Methods("POST")

	log.Fatal(http.ListenAndServe(":80", router))
}

func GetMessages(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(Messages)
}

func GetMessage(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, message := range Messages {
		idInt, err := strconv.Atoi(params["id"])
		if err != nil {
			http.Error(w, "ID is not an int", 400)
			return
		}

		if message.ID == idInt {
			json.NewEncoder(w).Encode(message)
			return
		}
	}

	http.Error(w, "Could not find message", 404)
}

func CreateMessage(w http.ResponseWriter, r *http.Request) {
	var messageString MessageString
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&messageString)
	if err != nil {
		http.Error(w, "Your JSON is probably malformed :[", 400)
		return
	}
	defer r.Body.Close()

	message := Message{ID: CurrentIndex, Message: messageString.Message}
	fmt.Printf("%+v", message)
	CurrentIndex++
	Messages = append(Messages, message)
	json.NewEncoder(w).Encode(Messages)
}
