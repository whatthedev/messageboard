# Messageboard
This is a small messageboard API/web app written during the Tuesday streams on [Kellen's channel](https://twitch.tv/Pothead_Pete).
There is a live example running at [streamapps.whatthedev.net](http://streamapps.whatthedev.net)

## API Documentation
The following API endpoints are currently implemented:

### Get all messages
* **URL:** /messages/
* **Method:** `GET`
* **URL Params:** none
* **Data Params:** none
* **Sample Call:** `coming soon`

### Get one message
* **URL:** /message/{id}
* **Method:** `GET`
* **URL Params:** id (message id, required)
* **Data Params:** none
* **Sample Call:** `coming soon`
* **Notes:** The message id auto increments on creation, so it is possible to get a subset of messages by iterating over the id's sequentially.

### Create a new message
* **URL:** /message/
* **Method:** `POST`
* **URL Params:** none
* **Data Params:**
```json
{
    "message": "foo"   
}
```
* **Sample Call:** `coming soon`

## Contact
You can contact us on the [What The Dev Discord](http://discord.whathedev.net)